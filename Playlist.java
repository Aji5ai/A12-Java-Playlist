import java.util.List;

public class Playlist {
    private Music currentMusic;
    private List<Music> musicList;

    
    public Playlist(Music currentMusic, List<Music> musicList) {
        this.currentMusic = currentMusic;
        this.musicList = musicList;
    }

    public void add(Music music){
        this.musicList.add(music);
    }

    public void remove(int position){
        this.musicList.remove(position);
    }

    public int getTotalDuration(){
        int totalDuration = 0;
        for (Music music : musicList) {
            totalDuration += music.getDuration();
        }
        return totalDuration;
    }

    public String next(){
        int currentIndex = musicList.indexOf(currentMusic);

        if (currentIndex == musicList.size() - 1){
            currentIndex=0;
        } else {
            currentIndex += 1;
        }

        currentMusic = musicList.get(currentIndex);

        return currentMusic.getTitle();
    }
}