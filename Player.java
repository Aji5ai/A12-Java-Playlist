import java.util.ArrayList;
import java.util.List;

public class Player {
    public static void main(String[] args) {
        // Création d'objets Artist
        Artist artist1 = new Artist("Rob", "Orbison");
        Artist artist2 = new Artist("Kate", "Bush");

        // Création d'objets Music
        Music music1 = new Music("Oh, Pretty Woman", 176, artist1.getFullName());
        Music music2 = new Music("Running Up That Hill", 296, artist2.getFullName());
        Music music3 = new Music("You Got It", 211, artist1.getFullName());

        // Affichage des informations des musiques
        System.out.println("\nInformations des musiques :");
        System.out.println(music1.getInfos());
        System.out.println(music2.getInfos());
        System.out.println(music3.getInfos());

        // Création d'une liste de musiques
        List<Music> musicList = new ArrayList<>();
        musicList.add(music1);
        musicList.add(music2);
        musicList.add(music3);

        // Création d'un objet Playlist
        Playlist playlist = new Playlist(music1, musicList);

        // Affichage de la durée totale de la playlist
        System.out.println("\nDurée totale de la playlist : " + playlist.getTotalDuration() + " secondes.");

        // Affichage du titre de la musique suivante dans la playlist
        System.out.println("Prochain titre de musique dans la playlist : " + playlist.next());

        // Ajout d'une nouvelle musique à la playlist
        Music music4 = new Music("Wuthering Heights", 225, artist1.getFullName());
        playlist.add(music4);
        System.out.println("Ajout de la musique : " + music4.getTitle());
        System.out.println("Durée totale de la playlist après ajout : " + playlist.getTotalDuration() + " secondes.");

        System.out.println("\nTitres de musique dans la playlist :");
        for (Music music : musicList) {
            System.out.println(music.getTitle());
        }
        // Suppression d'une musique de la playlist
        playlist.remove(3);
        System.out.println("Durée totale de la playlist après suppression : " + playlist.getTotalDuration() + " secondes.");

        System.out.println("\nTitres de musique dans la playlist après suppression :");
        for (Music music : musicList) {
            System.out.println(music.getTitle());
        }
    }
}
