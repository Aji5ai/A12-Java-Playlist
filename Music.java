public class Music {
    private String title;
    private double duration;
    private String artistSet;
    
    public Music(String title, double duration, String artistSet) {
        this.title = title;
        this.duration = duration;
        this.artistSet = artistSet;
    }

    public String getInfos(){
        int minutes = (int) this.duration / 60;
        int seconds = (int) this.duration % 60;
        return this.title + " " + String.format("%02d:%02d", minutes, seconds) + " " + this.artistSet;
    }

    public String getTitle(){
        return this.title;
    }

    public double getDuration(){
        return this.duration;
    }
}